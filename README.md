# Installation overview for Factory

A quick overview of Factory

## Introduction

This system represent a factory assembly line. There is an entry point (repository name: Entrypoint) where incoming jobs will be accepted and put on a queue. Here operational challenges like scalability, availability, uptime and single point of failure are central theme. Eventually the incoming jobs will be too much to handle, and the system administrator have to scale by creating new workers. 

## Servers

There are 5 applications required to set up. 

* Entrypoint
* Finished Job Entry System
* Safe
* Worker

Installation can be found as a README on each repository. 

## Deployment view

The deployment view is an representation of how it should look in the end. The number of workers are up to the system administrator.  

![alt tag](https://bytebucket.org/bscbookface/installation-guide-for-factory/raw/52d9c2116d94d0d28371f03f2c41e6404728e830/factory_deployment_view.png?token=350ad6f55139e0e3c708ec2263f99158e300e1a9)

